const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let root = document.createElement('div');
root.id = "root";
document.body.appendChild(root);
let list = document.createElement('ul');
root.appendChild(list);
let li;


function checkProperty(books) {
    const props = ['author', 'name', 'price'];
    let errorMessages = [];
    let innerList;

    books.forEach(item => {
        let keysArray = Object.keys(item);
        let difference = props.filter(key => !keysArray.includes(key));
        if (difference.length > 0) {
            difference.forEach((dif) => {
                errorMessages.push(`${JSON.stringify(item)} doesn't contains ${dif} \n`)
            });
        } else {
            innerList = document.createElement('ul');
            li = document.createElement('li');
            list.appendChild(li);
            li.appendChild(innerList);
            for (const key in item) {
                innerLi = document.createElement('li');
                innerList.appendChild(innerLi);
                innerLi.innerHTML = `${key}: ${item[key]}`;
            }
        }
    });

    const stringMessage = errorMessages.join();

    if (stringMessage) {
        throw new Error(stringMessage);
    }
}

try {
    checkProperty(books);
} catch (e) {
    console.log(e.message);
}


        // if (el.hasOwnProperty('name') && el.hasOwnProperty('price') && el.hasOwnProperty('author')) {
        //     li = document.createElement('li');
        //     li.innerHTML = `${el.name} blabla ${el.price} blablabla ${el.author}`;
        //     list.appendChild(li);
        // } else {
        //     throw new Error('this el doesnt include all props');
        // }